/**
 * 系统常量配置文件，命名规则
 * 平台核心：JE_模块_变量名
 * 业务常量：业务(MENU)_模块_变量名
 */

// 类型
// export const MENU_CORE_TYPE = [
//   { code: 'MENU', text: '模块', icon: 'jeicon jeicon-title' },
//   { code: 'MENU', text: '菜单', icon: 'fal fa-folder' },
//   { code: 'MT', text: '功能', icon: 'jeicon jeicon-function' },
//   { code: 'DIC', text: '字典功能', icon: 'fal fa-book' },
//   { code: 'IDDT', text: '插件', icon: 'fal fa-puzzle-piece' },
//   { code: 'CHART', text: '图表', icon: 'fal fa-chart-pie' },
//   // { code: 'REPORT', text: '报表', icon: 'jeicon jeicon-report' },
//   { code: 'PORTAL', text: '门户', icon: 'jeicon jeicon-portal' },
//   { code: 'URL', text: '链接', icon: 'fal fa-link' },
//   { code: 'IFRAME', text: 'IFRAME', icon: 'jeicon jeicon-iframe' },
// ];

// 快捷菜单图标背景
// export const MENU_CORE_BGCOLOR = [
//   { code: '#38BA72', text: '绿色' },
//   { code: '#52C6C6', text: '蓝色' },
//   { code: '#ED5858', text: '红色' },
//   { code: '#F28D48', text: '橙色' },
//   { code: '#9382FF', text: '紫色' },
//   { code: '#F5D540', text: '黄色' },
// ];

// 功能类型
// export const MENU_CORE_FUNCTYPE = [
//   { code: 'grid', text: '展示表格' },
//   { code: 'form', text: '展示表单' },
//   // { code: 'treegrid', text: '展示树形表格' },
// ];

// 流程模块
export const MENU_CORE_WFMODULE = [
  { code: 'XSXG', text: '销售相关' },
  { code: 'CYBG', text: '常用流程' },
  { code: 'JEDM', text: 'JEPaaSDemo' },
];
